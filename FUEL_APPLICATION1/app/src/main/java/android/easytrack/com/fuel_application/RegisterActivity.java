package android.easytrack.com.fuel_application;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private final AppCompatActivity activity = RegisterActivity.this;

    private NestedScrollView nestedScrollView;
    private TextInputLayout textInputLayoutName;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutPassword;
    private TextInputLayout textInputLayoutConfirmPassword;
    private TextInputEditText textInputEditTextName;
    private TextInputEditText textInputEditTextEmail;
    private TextInputEditText textInputEditTextPassword;
    private TextInputEditText textInputEditTextConfirmPassword;
    TextInputEditText mCompanyname,mTelephone,mBillingAddress,mCardNumber,expiry_date,ccv_number,mCardName,mBankName;
    RadioButton mRadioButton;
    TextView mtextview;
    private AppCompatButton appCompatButtonRegister;
    private AppCompatTextView appCompatTextViewLoginLink;
    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private User user;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initViews();
        initListeners();
        initObjects();
    }
    private void initViews() {
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        textInputLayoutName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        textInputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmPassword);
        textInputEditTextName = (TextInputEditText) findViewById(R.id.textInputEditTextName);
        textInputEditTextEmail = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);
        textInputEditTextConfirmPassword = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmPassword);
        mCompanyname=(TextInputEditText)findViewById(R.id.textInputEditTextCompanyName);
        mTelephone=(TextInputEditText) findViewById(R.id.TelePhone_no);
        mBillingAddress=(TextInputEditText) findViewById(R.id.Bill_address);
        mCardNumber=(TextInputEditText) findViewById(R.id.card_Number);
        expiry_date=(TextInputEditText) findViewById(R.id.Expiry_date);
        ccv_number=(TextInputEditText) findViewById(R.id.CCV_number);
        mCardName=(TextInputEditText)findViewById(R.id.card_Name);
        mBankName=(TextInputEditText)findViewById(R.id.Nick_name);
        checkBox=(CheckBox)findViewById(R.id.checkBoxRememberMe);
        //  Submit=(AppCompatButton)findViewById(R.id.Register_Submit_in_button);
       // mRadioButton=(RadioButton)findViewById(R.id.future_use);
       // mtextview=(AppCompatTextView)findViewById(R.id.useText);
        // backtoLogin=(AppCompatTextView)findViewById(R.id.loginText);
        appCompatButtonRegister = (AppCompatButton) findViewById(R.id.appCompatButtonRegister);

        appCompatTextViewLoginLink = (AppCompatTextView) findViewById(R.id.appCompatTextViewLoginLink);

    }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonRegister.setOnClickListener(this);
        appCompatTextViewLoginLink.setOnClickListener(this);

    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        inputValidation = new InputValidation(RegisterActivity.this);
        databaseHelper = new DatabaseHelper(RegisterActivity.this);
        user = new User();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.appCompatButtonRegister:
                if (!inputValidation.isInputEditTextFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_name))) {
                    return;
                }
                if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
                    return;
                }
                if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
                    return;
                }
                if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password))) {
                    return;
                }
                if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword, textInputEditTextConfirmPassword,
                        textInputLayoutConfirmPassword, getString(R.string.error_password_match))) {
                    return;
                }
                if (!databaseHelper.checkUser(textInputEditTextEmail.getText().toString().trim())) {

                    user.setName(textInputEditTextName.getText().toString().trim());
                    user.setEmail(textInputEditTextEmail.getText().toString().trim());
                    user.setCompanyName(mCompanyname.getText().toString().trim());
                    user.setPassword(textInputEditTextPassword.getText().toString().trim());
                    user.setTelePhone(mTelephone.getText().toString().trim());
                    user.setBillingaddress(mBillingAddress.getText().toString().trim());
                    user.setCardNumber(mCardNumber.getText().toString().trim());
                    user.setExpiryDate(expiry_date.getText().toString().trim());
                    user.setCcv(ccv_number.getText().toString().trim());
                    user.setCardName(mCardName.getText().toString().trim());
                    user.setBankName(mBankName.getText().toString().trim());
                    databaseHelper.addUser(user);

                    // Snack Bar to show success message that record saved successfully
                    Snackbar.make(nestedScrollView, getString(R.string.success_message), Snackbar.LENGTH_LONG).show();
                    emptyInputEditText();


                } else {
                    // Snack Bar to show error message that record already exists
                    Snackbar.make(nestedScrollView, getString(R.string.error_email_exists), Snackbar.LENGTH_LONG).show();
                }


                Intent submitIntent=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(submitIntent);
                break;

            case R.id.appCompatTextViewLoginLink:
                finish();
                break;
        }
    }
    private void postDataToSQLite() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_name))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return;
        }
        if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password))) {
            return;
        }
        if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword, textInputEditTextConfirmPassword,
                textInputLayoutConfirmPassword, getString(R.string.error_password_match))) {
            return;
        }
      /*  if (!inputValidation.isInputEditTextFilled(textInputEditTextCompanyName, mCompanyname,"Enter Password")) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(TelePhone_no, mTelephone, "Enter Telephone")){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(Bill_address,mBillingAddress, "Enter billing address")){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(card_Number, mCardNumber, "Enter card number")){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(CCV_number, ccv_number, "Enter ccv number")){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(Expiry_date, expiry_date, "Enter expiry date")){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(card_Name, mCardName, "Enter card name")){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(Nick_name, mBankName, "Enter bank name")){
            return;
        }*/

        if (!databaseHelper.checkUser(textInputEditTextEmail.getText().toString().trim())) {

            user.setName(textInputEditTextName.getText().toString().trim());
            user.setEmail(textInputEditTextEmail.getText().toString().trim());
            user.setCompanyName(mCompanyname.getText().toString().trim());
            user.setPassword(textInputEditTextPassword.getText().toString().trim());
            user.setTelePhone(mTelephone.getText().toString().trim());
            user.setBillingaddress(mBillingAddress.getText().toString().trim());
            user.setCardNumber(mCardNumber.getText().toString().trim());
            user.setExpiryDate(expiry_date.getText().toString().trim());
            user.setCcv(ccv_number.getText().toString().trim());
            user.setCardName(mCardName.getText().toString().trim());
            user.setBankName(mBankName.getText().toString().trim());
            databaseHelper.addUser(user);


            // Snack Bar to show success message that record saved successfully
            Snackbar.make(nestedScrollView, getString(R.string.success_message), Snackbar.LENGTH_LONG).show();
            emptyInputEditText();


        }




    }

    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        textInputEditTextName.setText(null);
        textInputEditTextEmail.setText(null);
        textInputEditTextPassword.setText(null);
        textInputEditTextConfirmPassword.setText(null);
        mCompanyname.setText(null);
        mTelephone.setText(null);
        mBillingAddress.setText(null);
        mCardNumber.setText(null);
        expiry_date.setText(null);
        ccv_number.setText(null);
        mCardName.setText(null);
        mBankName.setText(null);

    }

    }

