package android.easytrack.com.fuel_application;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

public class Home extends AppCompatActivity {
    private DrawerLayout mDraweLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar mtoolbar;
    NavigationView navigationView;
    String email1;
    Intent homeintent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle("");
        homeintent = getIntent();
        email1 = homeintent.getStringExtra("email_id");
        Log.d(homeintent.getStringExtra("email_id"), "Email3");
        initViews();
        initListeners();
    }

    private void initViews() {
        //  textViewName = (AppCompatTextView) findViewById(R.id.textViewName);
        // logbtn = (Button) findViewById(R.id.lgbutton);
        //  recyclerViewUsers = (RecyclerView) findViewById(R.id.recyclerViewUsers);
        mtoolbar = (Toolbar) findViewById(R.id.nav_action);
        mDraweLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDraweLayout, R.string.open, R.string.close);
        navigationView = (NavigationView) findViewById(R.id.design_navigation_view);
    }

    private void initListeners() {
        mDraweLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.My_Profile:
                        Intent intent_profile = new Intent(Home.this, UserListActivity.class);
                         intent_profile.putExtra("Email_id",email1);
                        startActivity(intent_profile);
                        //   mDraweLayout.closeDrawer();
                        break;
                    case R.id.new_logout:
                        new PrefManager(Home.this).isUserLogedOut();
                        Intent intent_logout = new Intent(Home.this, LoginActivity.class);
                        startActivity(intent_logout);
                        finish();
                        break;
                    case R.id.contact_us:
                        Intent intent_contact = new Intent(Home.this, Contact_Us.class);
                        startActivity(intent_contact);
                        finish();
                        break;


                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}