package android.easytrack.com.fuel_application;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;

/**
 * Created by HPLAP on 09-07-2017.
 */
public class PrefManager {
    Context context;
    PrefManager(Context context) {
        this.context = context;
    }


    public void saveLoginDetails(TextInputEditText textInputEditTextEmail, TextInputEditText textInputEditTextPassword) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Email", String.valueOf(textInputEditTextEmail));
        editor.putString("Password", String.valueOf(textInputEditTextEmail));
        editor.commit();
    }
    public String getEmail() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Email", "");
    }
    public boolean isUserLogedOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        boolean isEmailEmpty = sharedPreferences.getString("Email", "").isEmpty();
        boolean isPasswordEmpty = sharedPreferences.getString("Password", "").isEmpty();
        sharedPreferences.edit().clear().commit();
        return isEmailEmpty || isPasswordEmpty;
    }


}
