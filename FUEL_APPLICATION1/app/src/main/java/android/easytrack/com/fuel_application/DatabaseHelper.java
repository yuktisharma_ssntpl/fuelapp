package android.easytrack.com.fuel_application;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HPLAP on 08-07-2017.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "UserManager.db";

    // User table name
    private static final String TABLE_FUEL = "user";
    private static final String TABLE_CARD ="user_card";

    // User Table Columns names
    private String CREATE_FUEL_TABLE = "CREATE TABLE  " + TABLE_FUEL + "("
            + Database.COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Database.COLUMN_USER_NAME + " TEXT,"
            + Database.COLUMN_USER_EMAIL + " TEXT," + Database.COLUMN_USER_PASSWORD + " TEXT," + Database.COLUMN_USER_COMPANY + " TEXT," +   Database.COLUMN_USER_TELEPHONE + " TEXT," + Database.COLUMN_USER_ADDRESS + " TEXT" +")";


    private String CREATE_CARD_TABLE = "CREATE TABLE " + TABLE_CARD + "("  +Database.COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"  + Database.COLUMN_USER_NAME + " TEXT,"
            + Database.COLUMN_USER_EMAIL + " TEXT," + Database.COLUMN_USER_PASSWORD + " TEXT," + Database.COLUMN_CARD_NUMBER+ " TEXT," + Database.COLUMN_EXPIRY + " TEXT," +  Database.COLUMN_CCV_NUMBER + " TEXT," +Database.COLUMN_CARD_NAME + " TEXT," + Database.COLUMN_BANK_NAME + " TEXT" +")";
    // drop table sql query
    private String DROP_FUEL_TABLE = "DROP TABLE IF EXISTS " + TABLE_FUEL;
    private String DROP_CARD_TABLE = "DROP TABLE IF EXISTS " + TABLE_CARD;
    /**
     * Constructor
     *
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FUEL_TABLE);
        db.execSQL(CREATE_CARD_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_FUEL_TABLE);
        db.execSQL(DROP_CARD_TABLE);

        // Create tables again
        onCreate(db);

    }

    /**
     * This method is to create user record
     *
     * @param user
     */
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Database.COLUMN_USER_NAME, user.getName());
        values.put(Database.COLUMN_USER_EMAIL, user.getEmail());
        values.put(Database.COLUMN_USER_PASSWORD, user.getPassword());
        values.put(Database.COLUMN_USER_COMPANY, user.getCompanyName());
        values.put(Database.COLUMN_USER_TELEPHONE, user.getTelePhone());
        values.put(Database.COLUMN_USER_ADDRESS, user.getBillingaddress());
        db.insert(TABLE_FUEL, null, values);
        long id = db.insertWithOnConflict(TABLE_FUEL, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        ContentValues values1=new ContentValues();
        values1.put(Database.COLUMN_USER_NAME, user.getName());
        values1.put(Database.COLUMN_USER_EMAIL, user.getEmail());
        values1.put(Database.COLUMN_USER_PASSWORD, user.getPassword());
        values1.put(Database.COLUMN_CARD_NUMBER, user.getCardNumber());
        values1.put(Database.COLUMN_EXPIRY, user.getExpiryDate());
        values1.put(Database.COLUMN_CCV_NUMBER ,user.getCcv());
        values1.put(Database.COLUMN_CARD_NAME, user.getCardName());
        values1.put(Database.COLUMN_BANK_NAME,user.getBankName());
        db.insert(TABLE_FUEL, null, values1);


        // Inserting Row
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
    public List<User> getAllUser(String email) {
        // array of columns to fetch
        String[] columns = {
                Database.COLUMN_USER_ID,
                Database.COLUMN_USER_EMAIL,
                Database.COLUMN_USER_NAME,
                Database.COLUMN_USER_PASSWORD,
                Database.COLUMN_USER_COMPANY,
                Database.COLUMN_USER_TELEPHONE,
                Database.COLUMN_USER_ADDRESS,

        };
        // sorting orders
        String sortOrder =
                Database.COLUMN_USER_NAME + " ASC";
        List<User> userList = new ArrayList<User>();
        String selection = Database.COLUMN_USER_EMAIL + " = ?";
        String[] selectionArgs = {email};
        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_FUEL, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_ID))));
                Log.d(String.valueOf(user.getId()),"user_id");
                user.setName(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_EMAIL)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_PASSWORD)));
                user.setCompanyName(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_COMPANY)));
                user.setTelePhone(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_TELEPHONE)));
                user.setBillingaddress(cursor.getString(cursor.getColumnIndex(Database.COLUMN_USER_ADDRESS)));
             /*  user.setCardNumber(cursor.getString(cursor.getColumnIndex(COLUMN_CARD_NUMBER)));
                user.setExpiryDate(cursor.getString(cursor.getColumnIndex(COLUMN_EXPIRY)));
                user.setCcv(cursor.getString(cursor.getColumnIndex(COLUMN_CCV_NUMBER)));
                user.setCardName(cursor.getString(cursor.getColumnIndex(COLUMN_CARD_NAME)));
                user.setBankName(cursor.getString(cursor.getColumnIndex(COLUMN_BANK_NAME)));*/


                // Adding user record to list
             userList.add(user);
            } while (cursor.moveToNext());

        }
     /*  if (cursor1.moveToFirst()){
            do{
                User user=new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_USER_PASSWORD)));
                user.setCardNumber(cursor.getString(cursor.getColumnIndex(COLUMN_CARD_NUMBER)));
                user.setExpiryDate(cursor.getString(cursor.getColumnIndex(COLUMN_EXPIRY)));
                user.setCcv(cursor.getString(cursor.getColumnIndex(COLUMN_CCV_NUMBER)));
                user.setCardName(cursor.getString(cursor.getColumnIndex(COLUMN_CARD_NAME)));
                user.setBankName(cursor.getString(cursor.getColumnIndex(COLUMN_BANK_NAME)));
                userList.add(user);

            }while (cursor1.moveToNext());
        }
*/
        cursor.close();
       // cursor1.close();
        db.close();

        // return user list
        return userList;
    }
    /*
   /**
     * This method to update user record
     *
     * @param user
     */
    public void updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Database.COLUMN_USER_NAME, user.getName());
        values.put(Database.COLUMN_USER_EMAIL, user.getEmail());
        values.put(Database.COLUMN_USER_PASSWORD, user.getPassword());
        values.put(Database.COLUMN_USER_COMPANY, user.getCompanyName());
        values.put(Database.COLUMN_USER_TELEPHONE, user.getTelePhone());
        values.put(Database.COLUMN_USER_TELEPHONE, user.getBillingaddress());
        values.put(Database.COLUMN_USER_ADDRESS, user.getCardNumber());
        db.update(TABLE_FUEL, values, Database.COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        ContentValues values1 = new ContentValues();
        values1.put(Database.COLUMN_USER_NAME, user.getName());
        values1.put(Database.COLUMN_USER_EMAIL, user.getEmail());
        values1.put(Database.COLUMN_USER_PASSWORD, user.getPassword());
        values1.put(Database.COLUMN_CARD_NUMBER, user.getExpiryDate());
        values1.put(Database.COLUMN_EXPIRY, user.getCcv());
        values1.put(Database.COLUMN_CCV_NUMBER, user.getCardName());
        values1.put(Database.COLUMN_CARD_NAME, user.getBankName());

        // updating row
        db.update(TABLE_CARD, values,Database.COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method is to delete user record
     *
     * @param user
     */
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_FUEL, Database.COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.delete(TABLE_CARD, Database.COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @return true/false
     */
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                Database.COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = Database.COLUMN_USER_EMAIL + " = ?";

        // selection argument
        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_FUEL,//Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);
        //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();

        if (cursorCount > 0) {
            return true;
        }
        db.close();


        return false;
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @param password
     * @return true/false
     */
  public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                Database.COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = Database.COLUMN_USER_EMAIL + " = ?" + " AND " + Database.COLUMN_USER_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};


        Cursor cursor = db.query(TABLE_FUEL, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }


        cursor.close();


        return false;
    }

}
