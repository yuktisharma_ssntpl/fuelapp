package android.easytrack.com.fuel_application;

/**
 * Created by sword on 14-07-2017.
 */

public class Database {public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_USER_NAME = "user_name";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_USER_PASSWORD = "user_password";
    public static final String COLUMN_USER_COMPANY = "user_company";
    public static final String COLUMN_USER_TELEPHONE = "user_telephone";
    public static final String COLUMN_USER_ADDRESS = "user_address";
    public static final String COLUMN_CARD_NUMBER = "card_number";
    public static final String COLUMN_EXPIRY = "expiry_date";
    public static final String COLUMN_CCV_NUMBER = "ccv_number";
    public static final String COLUMN_CARD_NAME = "card_name";
    public static final String COLUMN_BANK_NAME = "bank_name";
}
