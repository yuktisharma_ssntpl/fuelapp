package android.easytrack.com.fuel_application;

import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by HPLAP on 08-07-2017.
 */
public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.UserViewHolder> {
    private List<User> listUsers;
    public UserRecyclerAdapter(List<User> listUsers) {
        this.listUsers=listUsers;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_recycler, parent, false);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.textViewName.setText(listUsers.get(position).getName());
        holder.textViewEmail.setText(listUsers.get(position).getEmail());
        holder.textViewPassword.setText(listUsers.get(position).getPassword());
        holder.textViewcompanyName.setText(listUsers.get(position).getCompanyName());
        holder.textViewtelephone.setText(listUsers.get(position).getTelePhone());
        holder.textViewaddress.setText(listUsers.get(position).getBillingaddress());
        holder.textViewcardNumber.setText(listUsers.get(position).getCardNumber());
        holder.textViewexpirydate.setText(listUsers.get(position).getExpiryDate());
        holder.textViewccvNo.setText(listUsers.get(position).getCcv());
        holder.textViewcardName.setText(listUsers.get(position).getCardName());
        holder.textViewbankName.setText(listUsers.get(position).getBankName());


    }


    @Override
    public int getItemCount()
    {
        Log.v(UserRecyclerAdapter.class.getSimpleName(),""+listUsers.size());
        return listUsers.size();

    }

        public class UserViewHolder extends RecyclerView.ViewHolder {
        public AppCompatEditText textViewName;
        public AppCompatEditText textViewEmail;
        public AppCompatEditText textViewPassword;
        public AppCompatEditText textViewcompanyName;
        public AppCompatEditText textViewtelephone;
        public AppCompatEditText textViewaddress;
        public AppCompatEditText textViewcardNumber;
        public AppCompatEditText textViewexpirydate;
        public AppCompatEditText textViewccvNo;
        public AppCompatEditText textViewcardName;
        public AppCompatEditText textViewbankName;




        public UserViewHolder(View view) {
            super(view);
            textViewName = (AppCompatEditText) view.findViewById(R.id.textViewName);
            textViewEmail = (AppCompatEditText) view.findViewById(R.id.textViewEmail);
            textViewPassword = (AppCompatEditText) view.findViewById(R.id.textViewPassword);
            textViewcompanyName=(AppCompatEditText) view.findViewById(R.id.textViewCompanyName);
            textViewtelephone=(AppCompatEditText) view.findViewById(R.id.textViewContactnumber);
            textViewaddress=(AppCompatEditText) view.findViewById(R.id.textViewAddress);
            textViewcardNumber=(AppCompatEditText) view.findViewById(R.id.textViewCardNumber);
            textViewexpirydate=(AppCompatEditText) view.findViewById(R.id.textViewexpiryDate);
            textViewccvNo=(AppCompatEditText) view.findViewById(R.id.textViewCCV);
            textViewcardName=(AppCompatEditText) view.findViewById(R.id.textViewCardName);
            textViewbankName=(AppCompatEditText) view.findViewById(R.id.textViewBakName);



        }


    }
}
